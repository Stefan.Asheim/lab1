package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022/2023. You can find
 * them here: https://inf100h22.stromme.me/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) 
    {
        ArrayList<ArrayList<Integer>> grid3 = new ArrayList<>();
        grid3.add(new ArrayList<>(Arrays.asList(1, 2, 3, 4)));
        grid3.add(new ArrayList<>(Arrays.asList(2, 3, 4, 1)));
        grid3.add(new ArrayList<>(Arrays.asList(3, 4, 1, 2)));
        grid3.add(new ArrayList<>(Arrays.asList(4, 1, 2, 3)));
        allRowsAndColsAreEqualSum(grid3);
    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) 
    {
        grid.remove(row);
    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) 
    {
        ArrayList<Integer> colSums = new ArrayList<>();
        ArrayList<Integer> rowSums = new ArrayList<>();
        int gridColumns = grid.size();

        for (int i = 0; i < gridColumns; i++) //Nested for loop adds the sums to two different lists
        {
            ArrayList<Integer> gridRow = grid.get(i);
            int rowSize = gridRow.size();
            int rowSum = 0;
            int colSum = 0;

            for (int j = 0; j < rowSize; j++)
            {
                rowSum += gridRow.get(j);
                int colNum = grid.get(j).get(i);
                colSum += colNum;
            }
            colSums.add(colSum);
            rowSums.add(rowSum);
        }

        int uniqCols = 0;
        int uniqRows = 0;

        for(int i = 0; i < colSums.size(); i++)
        {
            int refColSum = colSums.get(0);
            int refRowSum = rowSums.get(0);
            if(colSums.get(i) == refColSum)
            {
                uniqCols ++;
            }
            if(rowSums.get(i) == refRowSum)
            {
                uniqRows ++;
            }
        }

        if(uniqRows == colSums.size() && uniqCols == colSums.size())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}