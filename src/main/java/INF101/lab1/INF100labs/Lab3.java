package INF101.lab1.INF100labs;

/**
 * Implement the methods multiplesOfSevenUpTo, multiplicationTable and crossSum.
 * These programming tasks was part of lab3 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/3/
 */
public class Lab3 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        multiplesOfSevenUpTo(49);
        multiplicationTable(3);
        System.out.println(crossSum(12));
    }

    public static void multiplesOfSevenUpTo(int n) 
    {
        for (int i = 7; i<=n; i= i + 7)
        {
            System.out.println(i);
        }
    }

    public static void multiplicationTable(int n) {
        for (int i = 1; i <=n; i++)
        {
            System.out.print(i + ": ");
            for (int j = 1; j <=n; j++)
            {
                System.out.print(i*j + " ");
            }
            System.out.println();
        }
    }

    public static int crossSum(int num) 
    {
        String number = String.valueOf(num);
        int sum = 0;
        for (int i = 0; i < number.length(); i++)
        {
            char siffer = number.charAt(i);
            sum += Character.getNumericValue(siffer);
        }
        return sum;   
    }

}