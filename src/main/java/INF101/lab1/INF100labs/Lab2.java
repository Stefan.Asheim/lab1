package INF101.lab1.INF100labs;

/**
 * Implement the methods findLongestWords, isLeapYear and isEvenPositiveInt.
 * These programming tasks was part of lab2 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/2/
 */
public class Lab2 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        findLongestWords("Game", "Action", "Champion");
        System.out.println(isLeapYear(2022));
    }

    public static void findLongestWords(String word1, String word2, String word3) {
        int lenWordOne = word1.length();
        int lenWordTwo = word2.length();
        int lenWordThree = word3.length();

        if(lenWordOne > lenWordTwo && lenWordOne > lenWordThree)
        {
            System.out.println(word1);
        }
        else if (lenWordTwo > lenWordOne && lenWordTwo > lenWordThree)
        {
            System.out.println(word2);
        }
        else if (lenWordThree > lenWordOne && lenWordThree > lenWordTwo)
        {
            System.out.println(word3);
        }
        else if(lenWordOne == lenWordTwo && lenWordTwo == lenWordThree)
        {
            System.out.println(word1);
            System.out.println(word2);
            System.out.println(word3);
        }
        else if (lenWordOne == lenWordTwo)
        {
            System.out.println(word1);
            System.out.println(word2);
        }
        else if (lenWordOne == lenWordThree)
        {
            System.out.println(word1);
            System.out.println(word3);
        }
        else if (lenWordTwo == lenWordThree)
        {
            System.out.println(word2);
            System.out.println(word3);
        }
    }

    public static boolean isLeapYear(int year) 
    {
        if(year % 4 == 0)
        {
            if (year%100 == 0)
            {
                if (year%400==0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
        }
        else 
        {
            return false;
        }
      
    }

    public static boolean isEvenPositiveInt(int num) 
    {
        if(num > 0 && num % 2 == 0)
        {
            return true;
        }
        else 
        {
            return false;
        }
    }

}