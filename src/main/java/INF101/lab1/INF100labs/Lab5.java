package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Implement the methods removeThrees, uniqueValues and addList.
 * These programming tasks was part of lab5 in INF100 fall 2022/2023. You can find them here: https://inf100h22.stromme.me/lab/5/
 */
public class Lab5 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        ArrayList<Integer> list1 = new ArrayList<>(Arrays.asList(1, 1, 2, 1, 3, 3, 3, 2));
        ArrayList<Integer> removedList1 = uniqueValues(list1);
        System.out.println(removedList1);
    }

    public static ArrayList<Integer> multipliedWithTwo(ArrayList<Integer> list) 
    {
        ArrayList <Integer> newList = new ArrayList<>();
        for(int i = 0; i < list.size(); i++)
        {
            int num = 2 * list.get(i);
            newList.add(num);
        } 
        return newList;
    }

    public static ArrayList<Integer> removeThrees(ArrayList<Integer> list) 
    {
        for(int i = list.size() -1; i >= 0; i--)
        {
            int num = list.get(i);
            if (num == 3)
            {
                list.remove(i);
            }
        }
        return list;
    }

    public static ArrayList<Integer> uniqueValues(ArrayList<Integer> list) 
    {
        ArrayList<Integer> uniList = new ArrayList<>();
        int size = list.size();

        for (int i = 0; i < size; i++)
        {
            int current = list.get(i);
            boolean inList = contains(uniList, current);
            if(!inList)
            {
                uniList.add(current);
            }
        }
        return uniList;
    }

    public static boolean contains(ArrayList<Integer> list, int value) 
    {
        for (int i = 0; i < list.size(); i++)
        {
            if(list.get(i) == value)
            {
                return true;
            }
        }
        return false;
    }

    public static void addList(ArrayList<Integer> a, ArrayList<Integer> b) {
        int listASize = a.size(); //Assumes that a and b are equal sizes


        for(int i =0; i < listASize; i++)
        {
            int numA = a.get(i);
            int numB = b.get(i);
            int newNum = numA + numB;
            a.remove(i);
            a.add(i,newNum);
        }
    }

}